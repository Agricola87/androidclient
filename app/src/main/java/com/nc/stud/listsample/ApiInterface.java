package com.nc.stud.listsample;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiInterface {
//    @GET("/androidcommand?command=onetask&user=u3&pass=p3")
//    Call<OneItem> getOneItem();

//    @GET("/androidcommand?command=init&user=u3&pass=p3")
//    Call<TodoList> getList();

    @GET("/androidcommand")
    Call<TodoList> getList(@Query("user") String username, @Query("pass") String password, @Query("command") String commandName);


}

///androidcommand?command=onetask&user=u2&pass=p3