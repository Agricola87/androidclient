package com.nc.stud.listsample;

interface ListItemClickHandler {
    void onListItemClick(int position);

    void onListItemLongClick(int position);
}
