package com.nc.stud.listsample;

import android.support.annotation.NonNull;
import android.support.v4.util.Pair;
import android.support.v7.widget.RecyclerView;
import android.view.*;
import android.widget.TextView;

import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
    private List<Pair<String, String>> mDataset;
    private ListItemClickHandler mClickHandler;


    public void setData(List<Pair<String, String>> myDataset) {
        mDataset = myDataset;
        notifyDataSetChanged();
    }

    public void addItem(Pair<String, String> record) {
        mDataset.add(record);
        notifyDataSetChanged();
    }

    public void setItem(int position, Pair<String, String> record) {
        mDataset.set(position, record);
        notifyDataSetChanged();
    }

    public Pair<String, String> getItem(int position) {
        return mDataset.get(position);
    }

    public void removeItem(int position) {
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    // Create new views (invoked by the layout manager)
    @Override
    @NonNull
    public MyAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.todo_list_item, parent, false);
        return new MyViewHolder(v, mClickHandler);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.mTextView.setTag(mDataset.get(position).first);
        holder.mTextView.setText(mDataset.get(position).second);
    }

    public void setClickHandler(ListItemClickHandler clickHandler) {
        mClickHandler = clickHandler;
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    static class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView mTextView;
        private ListItemClickHandler mClickHandler;

        MyViewHolder(View v, ListItemClickHandler clickHandler) {
            super(v);
            mClickHandler = clickHandler;
            mTextView = v.findViewById(R.id.item_text_view);
            setupClickListeners(mTextView);
        }

        private void setupClickListeners(View view) {
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mClickHandler.onListItemClick(getAdapterPosition());
                }
            });
            view.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    mClickHandler.onListItemLongClick(getAdapterPosition());
                    return true;
                }
            });
        }
    }
}