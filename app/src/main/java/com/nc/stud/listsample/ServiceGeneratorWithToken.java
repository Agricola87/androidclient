package com.nc.stud.listsample;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ServiceGeneratorWithToken {

    public static final String API_BASE_URL = "https://192.168.1.100:8443/";


    public /*static*/ <S> S createService(Class<S> serviceClass) {
        return createService(serviceClass, null);
    }

    public /*static*/ <S> S createService(
            Class<S> serviceClass, final String authToken) {
        Log.e("Generator", "createService called with " + authToken);
        if (TextUtils.isEmpty(authToken)) {
            return null;
        }


        OkHttpClient.Builder httpClientBuilder = new OkHttpClient.Builder();

        AuthenticationInterceptor interceptor = new AuthenticationInterceptor(authToken);
        httpClientBuilder.addInterceptor(interceptor);

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor(
                new HttpLoggingInterceptor.Logger() {
                    @Override public void log(@NonNull String message) {
                        Log.e("REST", message);
                    }
                }).setLevel(HttpLoggingInterceptor.Level.BODY);

        httpClientBuilder.addInterceptor(logging);

        httpClientBuilder.hostnameVerifier(new HostnameVerifier() {
            @Override
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        });

        try {
            X509TrustManager trustManager = getTrustManager();
            httpClientBuilder.sslSocketFactory(getSslSocketFactory(trustManager), trustManager);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create());

        builder.client(httpClientBuilder.build());
        Retrofit retrofit = builder.build();

        return retrofit.create(serviceClass);

    }

    public class AuthenticationInterceptor implements Interceptor {

        private String authToken;

        public AuthenticationInterceptor(String token) {
            this.authToken = token;
        }

        @Override
        public Response intercept(Chain chain) throws IOException {
            Request original = chain.request();

            Request.Builder builder = original.newBuilder()
                    .header("X-CSRF-TOKEN", authToken)
                    .header("x-requested-with", "XMLHttpRequest");



            Request request = builder.build();
            Response response = chain.proceed(request);
//            Log.i(this.getClass().getSimpleName()
//                    , response.toString() +
//                            response.message() +
//                            " http code: " + String.valueOf(response.code())
//            );
            return chain.proceed(request);
        }
    }

    @NonNull
    private static X509TrustManager getTrustManager() {
        return new X509TrustManager() {
            @SuppressLint("TrustAllX509TrustManager")
            @Override
            public void checkClientTrusted(X509Certificate[] chain, String authType) {
            }

            @SuppressLint("TrustAllX509TrustManager")
            @Override
            public void checkServerTrusted(X509Certificate[] chain, String authType) {
            }

            @Override
            public X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[]{};
            }
        };
    }

    private static SSLSocketFactory getSslSocketFactory(TrustManager trustManager) throws NoSuchAlgorithmException, KeyManagementException {
        // TLSv1.2 - security team requirement, do not change without approval
        final SSLContext sslContext = SSLContext.getInstance("TLS");
        sslContext.init(null, new TrustManager[]{trustManager}, new SecureRandom());
        return sslContext.getSocketFactory();
    }
}