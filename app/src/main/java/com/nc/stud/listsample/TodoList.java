package com.nc.stud.listsample;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.HashMap;

public class TodoList {
    @SerializedName("showMode")
    private String showMode;

    @SerializedName("title")
    private String title;

    @SerializedName("taskType")
    private String taskType;

    @SerializedName("items")
    private ArrayList<HashMap<String, String>> items;

    public String getShowMode() {
        return showMode;
    }

    public String getTaskType() {
        return taskType;
    }

    public ArrayList<HashMap<String, String>> getItems() {
        return items;
    }

    public String getTitle() {
        return title;
    }
}
