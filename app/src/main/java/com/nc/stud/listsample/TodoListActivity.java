package com.nc.stud.listsample;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;

import static android.content.DialogInterface.BUTTON_NEUTRAL;
import static android.content.DialogInterface.BUTTON_POSITIVE;

public class TodoListActivity extends AppCompatActivity implements ListItemClickHandler {

    public static final String CREATE = "CREATE";
    private EditText mDialogContentView;
    private AlertDialog mSimpleDialog;
    private RecyclerView mRecyclerView;
    private MyAdapter mAdapter;
    private static int hackIndex = 0;
    private int mCurrentTodoItemPosition = -1;

    private String userName;
    private String password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_todo_list);

        Intent intent = getIntent();
        userName = intent.getStringExtra("name");
        password = intent.getStringExtra("pass");

        mDialogContentView = (EditText)View.inflate(this, R.layout.dialog_edit, null);

        setupListView();

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(createAddClickListener());
    }

    private void setupListView() {
        mRecyclerView = findViewById(R.id.list_todo);
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        mAdapter = new MyAdapter();
        mRecyclerView.setAdapter(mAdapter);

        //
        MyListTask mlt = new MyListTask();
        mlt.execute();
        while (!mlt.isDone()){
            ;
        }
        TodoList tl = mlt.getTodoList();
        ArrayList<HashMap<String, String>> items = tl.getItems();
        ArrayList<Pair<String, String>> listToSet = new ArrayList<>();
        for(HashMap<String, String> item : items){
            listToSet.add(new Pair<>("", item.get("title")));
        }
        mAdapter.setData(listToSet);

//        mAdapter.setData(new ArrayList<Pair<String, String>>());
//        mAdapter.setData(new ArrayList<Pair<String, String>>(){{add(new Pair<>("", "One"));}});

        mAdapter.setClickHandler(this);
    }

//    private ArrayList<String> getInitList(){
//
//    }

    @Override
    public void onListItemClick(int position) {
        mCurrentTodoItemPosition = position;
        Pair<String, String> todoItem = mAdapter.getItem(position);
        showSimpleDialog(todoItem.second);
    }

    @Override
    public void onListItemLongClick(int position) {
        mAdapter.removeItem(position);
        mCurrentTodoItemPosition = -1;
    }

    @NonNull
    private View.OnClickListener createAddClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showSimpleDialog();
            }
        };
    }

    private void onFinishEditDialog(String inputText) {
        if (mCurrentTodoItemPosition < 0) {
            mAdapter.addItem(new Pair<>(String.valueOf(hackIndex++), inputText));
        } else {
            Pair<String, String> newTodoItem = new Pair<>(mAdapter.getItem(mCurrentTodoItemPosition).first, inputText);
            mAdapter.setItem(mCurrentTodoItemPosition, newTodoItem);
            mCurrentTodoItemPosition = -1;
        }
    }

    private void showSimpleDialog() {
        showSimpleDialog("");
    }

    private void showSimpleDialog(String innerText) {
        if (mSimpleDialog == null) {
            mSimpleDialog = createSimpleDialog();
        }
        mDialogContentView.setText(innerText);
        mSimpleDialog.show();
    }

    private AlertDialog createSimpleDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Todo:");
        DialogInterface.OnClickListener buttonListener = createOnClickListener();
        return builder
                .setView(mDialogContentView)
                .setPositiveButton(android.R.string.ok, buttonListener)
                .setNegativeButton(android.R.string.cancel, buttonListener)
                .create();
    }

    @NonNull
    private DialogInterface.OnClickListener createOnClickListener() {
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (BUTTON_POSITIVE == which) {
                    onFinishEditDialog(mDialogContentView.getText().toString());
                } else if (BUTTON_NEUTRAL == which) {
                    mCurrentTodoItemPosition = -1;
                }
                dialog.dismiss();
            }
        };
    }

    class MyListTask extends AsyncTask<Void, Void, Void> {

        TodoList todoList;
        boolean isDone = false;

        public boolean isDone() {
            return isDone;
        }

        public TodoList getTodoList() {
            return todoList;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
        @Override
        protected Void doInBackground(Void... params) {
            ServiceGeneratorWithToken sgwt = new ServiceGeneratorWithToken();
            ApiInterface apiInterface = sgwt.createService(ApiInterface.class, "notoken");
            Call<TodoList> call = apiInterface.getList(userName, password, "init");


            isDone = false;
            try {
                todoList = call.execute().body();
//                Log.e("Success? = ", "Success = " + call.execute().isSuccessful());
                Log.e("ITEM", "Todo item = " + todoList.getTaskType());
                isDone = true;
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

        }
    }

}
